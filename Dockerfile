FROM node:lts-alpine

LABEL maintainer="Satit Rianpit <rianpit@gmail.com>"

RUN apk update && apk upgrade

RUN apk add --no-cache git nginx \
  g++ gcc libgcc make python

RUN apk add tzdata \
  && cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
  && echo "Asia/Bangkok" > /etc/timezone
  
RUN npm i -g pm2 ts-node typescript nodemon

RUN tsc --version

EXPOSE 80